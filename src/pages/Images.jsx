import { useState, useEffect } from 'react'

import Button from '@mui/material/Button';
import SearchIcon from '@mui/icons-material/Search';

import App from '../components/app/app'
import Section from '../components/common/section'
import Input from '../components/common/input'
import Table from '../components/common/table'

import Swal from 'sweetalert2'

import ImagesService from '../services/images.service'

const Images = () => {
  const [patient, setPatient] = useState(null)
  const [patientImages, setPatientImages] = useState('')
  const [isSearch, setIsSearch] = useState(false)
  const [isDataValue, setIsDataValue] = useState(false)

  useEffect(() => {
    if (!isSearch || !patient) {
      getAllImagesAPI()
    }
  }, [])

  const getAllImagesAPI = async () => {
    const resultAllImages  = await ImagesService.getAllImages(patient)
    console.log(resultAllImages.message)
    if (!resultAllImages.isData) {
      if (resultAllImages.message !== 'Get All User') {
        Swal.fire({
          icon: "error",
          title: "System Expire!",
          text: resultAllImages.res_desc,
        });
      }

      setIsDataValue(false)
      setPatientImages('')
    } else {
      setPatientImages(resultAllImages.data)
      setIsDataValue(true)
    }
  }

  const handleOnClickSearch = async () => {
    setIsSearch(true)

    const resultFilterImages  = await ImagesService.getSomethingImages(patient)
    if (!resultFilterImages.isData) {
      if (resultFilterImages.message !== 'Get Something User') {
        Swal.fire({
          icon: "error",
          title: "System Expire!",
          text: resultFilterImages.res_desc,
        });
      }
      setIsDataValue(false)
      setPatientImages('')
    } else {  
      setPatientImages(resultFilterImages.data)
      setIsDataValue(true)
    }
    setPatient('')
  }

  const onDelImages = (imageName) => {
    Swal.fire({
      title: "คุณต้องการลบรูปภาพ"+ imageName + "ใช่หรือไม่",
      showDenyButton: true, 
    }).then((result) => {
      if (result.isConfirmed) {
        ImagesService.delSomethingImages(imageName).then((res) => {
          if (res.isDel) {
            Swal.fire("ลบรูปภาพ!", "",  "success");
            getAllImagesAPI()
          } else {
            Swal.fire({
              icon: "error",
              title: "มีบางอย่างผิดพลาด",
              text: res.messageDel,
            });
          }
        })
       
      }
    });
  }

  return (
    <div>
      <App>
        <Section
          title={ 'Smart EKG 1.4.9' }
          margin='10px 0px 50px 0px'
          style={{ backgroundColor : '#FFF'}}
          >
            <Input
              label={ 'ค้นหาเลขประจำตัวผู้ป่วย (HN) ตัวอย่างเช่น 3, 5' }
              type='text'
              value={ patient }
              onChange={(text) => {setPatient(text)}}
              required={ true }
              maxLength={100}
            />
            <Button
              variant="contained" 
              color="primary" 
              size="large"
              onClick={ () => handleOnClickSearch() }
              startIcon={<SearchIcon />}
            > ค้นหา </Button>
        </Section>
        <Table data={patientImages} dataLength={patientImages.length} isDataValue={isDataValue} onDelImage={onDelImages}/>
      </App>
    </div>
  )
}
export default Images