import PropTypes from 'prop-types'

const Section = (props) => {
  return (
    <div className='section' style={ props.style }>
      <div className='title' hidden={ !props.title }>{ props.title }</div>
      <div className='subtitle'>{ props.subtitle }</div>
      <div className='hr' hidden={ !props.title } />
      { props.children }
      <div
        className='close'
        hidden={ props.isCloseable ? !props.isCloseable : true }
        onClick={ props.onClickClose }
      ><i className='icon-cancel-circled' /></div>
      <style>
        {
          `
            .section {
              position: relative;
              margin: ${props.margin ? props.margin : '0px'};
              width: ${props.width ? props.width : 'auto'};
              flex-grow: ${props.grow ? props.grow : props.width ? 0 : 1};
              flex-basis: ${props.basis ? props.basis : props.width ? 'auto' : 0};
              animation: ${props.animation ? props.animation : 'none'};
              padding: 20px;
              box-sizing: border-box;
              border-radius: 10px;
              box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.08);
              min-width: 0;
              display: ${props.hidden ? 'none' : 'block'};
            }
            .section .title {
              font-size: 0.95em;
            }
            .section .subtitle {
              line-height: 1.3;
              font-size: 0.8em;
            }
            .section .hr {
              padding-bottom: 5px;
              margin-bottom: 10px;
            }
            .section .close {
              position: absolute;
              top: -10px;
              right: -10px;
              font-size: 20px;
            }
            .section .close:hover {
              cursor: pointer;
            }
          `
        }
      </style>
    </div>
  )
}

Section.propTypes = {
  theme: PropTypes.object,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  isCloseable: PropTypes.bool,
  margin: PropTypes.string,
  width: PropTypes.string,
  grow: PropTypes.string,
  basis: PropTypes.string,
  animation: PropTypes.string,
  onClickClose: PropTypes.func,
  children: PropTypes.any,
  hidden: PropTypes.bool,
  style: PropTypes.object
}

export default Section
