import { useState } from 'react'

import {
  IconButton,
  Paper,
  Box,
  styled,
  Table,
  TableBody,
  TableCell,
  tableCellClasses,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  TableFooter
} from '@mui/material';


import PanoramaTwoToneIcon from '@mui/icons-material/PanoramaTwoTone';
import DeleteForeverTwoToneIcon from '@mui/icons-material/DeleteForeverTwoTone';
import ErrorOutlineTwoToneIcon from '@mui/icons-material/ErrorOutlineTwoTone';

const table = (props) => {
  const { data, dataLength, isDataValue, onDelImage } = props

  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [page, setPage] = useState(0)
  
  const onClickNewTab = (image, name) => {
    var newTab = window.open('', name);
    newTab.document.body.innerHTML = `<img src="${image}">`
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - dataLength) : 0;

  const StyledTableCell = styled(TableCell)(() => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: '#D6DBDF',
      color: '#212121',
      fontWeight: 'bold'
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 10,
    },
  }));

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <StyledTableCell >Name</StyledTableCell >
                <StyledTableCell  align="right">Tools</StyledTableCell > 
              </TableRow>
            </TableHead>
            <TableBody>
                {
                  isDataValue ?
                    data && (rowsPerPage > 0
                      ? data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      : data
                    ).map((value, index) => (
                      <StyledTableRow
                        key={index}
                        // sx={{ '&:last-child td, &:last-child th': { border: 1 } }}
                      >
                        <TableCell>
                          { value.name }
                        </TableCell>
                        <TableCell align="right">
                          <IconButton size='small' onClick={ () => onClickNewTab(`data:image/png;base64,${value.base64}`, value.name)}>
                          <PanoramaTwoToneIcon color="primary"/>
                          </IconButton>
                          <IconButton size='small' onClick={() => onDelImage(value.name)}>
                            <DeleteForeverTwoToneIcon color="error"/>
                          </IconButton>
                        </TableCell>
                      </StyledTableRow>
                    )) : 
                    (
                      <StyledTableRow style={{ height: 100}}> 
                        <TableCell colSpan={2}  align="center">
                          <div className='no-result'>
                            <ErrorOutlineTwoToneIcon />
                            <div className='message'>ไม่พบรายการ</div>
                          </div>
                        </TableCell>
                      </StyledTableRow>
                    )
                }
                 {emptyRows > 0 && (
                  <StyledTableRow style={{ height: 53 * emptyRows }}> 
                    <TableCell colSpan={2} />
                  </StyledTableRow>
                )}
                  </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[ 10, 15, 25, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={dataLength}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  slotProps={{
                    select: {
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    },
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </Paper>
    </Box>
  )
}

export default table