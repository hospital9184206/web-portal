import PropTypes from 'prop-types'

const Default = (props) => {
  const {
    onChange
  } = props
  return (
    <div className={ props.disabled ? 'input disabled' : 'input' }>
      <input
        name={ props.name }
        type={ props.type }
        value={ props.value }
        onChange={ (e) => onChange(e.target.value) }
        placeholder={ props.placeholder }
        minLength={ props.minLength }
        maxLength={ props.maxLength }
        min={ props.min }
        max={ props.max }
        step={ props.step }
        pattern={ props.pattern }
        title={ props.title }
        autoComplete={ props.autoComplete }
        disabled={ props.disabled || false }
        required={ props.required || false }
      />
    </div>
  )
}

Default.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number,
  minLength: PropTypes.number,
  maxLength: PropTypes.number,
  step: PropTypes.number,
  pattern: PropTypes.string,
  title: PropTypes.string,
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  placeholder: PropTypes.string
}

const Input = (props) => {
  return (
    <div className='input-wrapper'>
      <label>{ props.label }</label>
      <Default { ...props } />
      <style>
      {
          `
            .input-wrapper {
              display: ${props.hidden ? 'none' : 'block'};
              width: calc(100% - 10px);
              margin: 5px;
            }
            .input-wrapper label {
              font-size: 0.85em;
              color: #97a4ba;
              line-height: 2;
            }
            .input-wrapper .input input {
              position: relative;
              width: 100%;
              min-height: 35px;
              box-sizing: border-box;
              border: 1px solid #e3e3e3;
              border-radius: 10px;
              background-color: #ffffff;
              padding-top: 4px;
            }
            .input-wrapper :global(.input:focus-within) {
              border: 1px solid #1d4cf0;
            }
            .input-wrapper :global(.input::placeholder) {
              color: #a3a3a3;
            }
            .input-wrapper :global(.input.disabled) {
              border: 1px solid #e3e3e3;
              background-color: #fafafa;
            }
            .input-wrapper :global(input) {
              font-size: 0.875em;
              width: 100%;
              height: 100%;
              border: 0px;
              padding: 0px 10px;
              background-color: transparent;
              color: 1f1f1f;
              box-sizing: border-box;
            }
          `
        }
      </style>
    </div>
  )
}

Input.propTypes = {
  theme: PropTypes.object,
  label: PropTypes.any,
  hidden: PropTypes.bool
}

export default Input
