import PropTypes from 'prop-types'

const Group = (props) => {
  return (
    <div className='group' style={props.style}>
      {props.children}
      <style>
        {`
          .group {
            display: ${props.hidden ? 'none' : 'flex'};
            margin: ${props.margin ? props.margin : '0px'};
            width: ${props.width ? props.width : 'auto'};
            flex-direction: ${props.direction ? props.direction : 'column'};
            flex-wrap: ${props.wrap ? props.wrap : 'nowrap'};
            flex-basis: ${props.basis ? props.basis : 0};
            justify-content: ${props.justifyContent ? props.justifyContent : 'flex-start'};
            align-items: ${props.alignItems ? props.alignItems : 'normal'};
          }

          @media (min-width: 1280px) {
            .group {
              display: ${props.hidden ? 'none' : 'flex'};
              margin: ${props.margin ? props.margin : '0px'};
              width: ${props.width ? props.width : 'auto'};
              flex-grow: ${props.grow ? props.grow : 1};
              flex-direction: ${props.direction ? props.direction : 'row'};
              flex-wrap: ${props.wrap ? props.wrap : 'nowrap'};
              flex-basis: ${props.basis ? props.basis : 0};
              justify-content: ${props.justifyContent ? props.justifyContent : 'flex-start'};
              align-items: ${props.alignItems ? props.alignItems : 'normal'};
            }
          }
        `}
      </style>
    </div>
  )
}

Group.propTypes = {
  margin: PropTypes.string,
  width: PropTypes.string,
  grow: PropTypes.string,
  direction: PropTypes.string,
  wrap: PropTypes.string,
  basis: PropTypes.string,
  justifyContent: PropTypes.string,
  alignItems: PropTypes.string,
  hidden: PropTypes.bool,
  style: PropTypes.object,
  children: PropTypes.any
}

export default Group
