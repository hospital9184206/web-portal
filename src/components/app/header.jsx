const Header = () => {

  return (
    <div id='header'>
      <style>
        {
          `
            #header {
              position: fixed;
              top: 0px;
              left: 0px;
              display: flex;
              align-items: center;
              justify-content: space-between;
              width: 100%;
              height: 60px;
              padding: 15px 20px;
              box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.08);
              z-index: 2;
              box-sizing: border-box;
              background-color: #FFF;
            }
          `
        }
      </style>
    </div>
  )
}

export default Header
