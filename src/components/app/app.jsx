import Header from './header'

const Layout = (props) => {
  const { children } = props
  return (
    <>
      <div className='layout'>
        <Header />
        <div className='wrapper'>
          <div className='container'>
            { children }
          </div>
        </div>
        <style>
          {
            `
              .layout {
                width: 100%;
                height: 100%;
              }
              .layout .wrapper {
                width: 100%;
                height: 100%;
                padding-top: 60px;
                box-sizing: border-box;
              }
              .layout .container {  
                padding: 40px;
                box-sizing: border-box;
              }
            `
          }
        </style>
      </div>
    </>
  )
}

// Layout.propTypes = {
//   language: PropTypes.string,
//   paymentProvider: PropTypes.object,
//   user: PropTypes.object,
//   title: PropTypes.string,
//   children: PropTypes.any
// }

export default Layout
