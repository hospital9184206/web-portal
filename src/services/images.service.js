import axios from "axios";

const API_URL = "http://localhost:3000/images";

const getAllImages = () => {
  return axios.get(API_URL).then((response) => {
    return response.data;
  });
};

const getSomethingImages = (textValue) => {
  return axios.post(API_URL, {textValue}).then((response) => {
    return response.data;
  });
};

const delSomethingImages = (textValue) => {
  return axios.post(API_URL+'/delete', {textValue}).then((response) => {
    return response.data;
  });
};

const ImagesService = {
  getAllImages,
  getSomethingImages,
  delSomethingImages
};

export default ImagesService;